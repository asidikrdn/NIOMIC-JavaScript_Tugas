let variabelSatu = 23;
let variabelDua = "Ini adalah string";
let variabelTiga = true;
let variabelEmpat = null;
let variabeLima = undefined;

let siswa = ['Ahmad', 'Sidik', 'Rudini', 'Rizki', 'Muhammad'];

console.log(variabelSatu);
console.log(variabelDua);
console.log(variabelTiga);
console.log(variabelEmpat);
console.log(variabeLima);
console.log(siswa);